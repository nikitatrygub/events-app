import React from 'react';
import { Switch , Route, Redirect } from 'react-router-dom';
import Home from './containers/Home/Home';


const App: React.FC = () => {

  const categories = ['places' , 'events' , 'realestate'];
  
  const mapCategoriesToRoute = () => {
    return categories && categories.map((category , i) => (
        <Route key={i} exact path={`/explorer/${category}`} render={() => <Home category={category} />} />
    ))
}
  return (
      <Switch>
        <Route 
          exact 
          path="/" 
          render={()=><Redirect to={`explorer/${categories[0]}`} />} 
        />
        {mapCategoriesToRoute()}
      </Switch>
  );
}

export default App;
