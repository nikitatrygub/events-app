import React, { Fragment } from 'react'
import { HomeTabs } from '../../components/HomeTabs/HomeTabs';
import { Layout } from '../../components/Layout/Layout';
import { PlacesFilter } from '../../components/PlacesFilter/PlacesFilter';
import { CardListHeader } from '../../components/CardListHeader/CardListHeader';
import { loadPlacesAction } from '../../store/places/actions';
import { PlaceCardList } from '../../components/PlaceCardList/PlaceCardList';
import { MapContainer } from '../../components/Map/Map';
import { getDataByCategory } from './Home.helpers';
import { connect } from 'react-redux';

interface HomeProps {
    category?:string
    places?:object
}

class Home extends React.Component<HomeProps>{

    shouldComponentUpdate(prevProps:any , nextProps:any){
        if(prevProps !== nextProps)
            return true;
        else
            return false;
    }

    componentDidMount(){
        const { category , api , places }:any = this.props;
        const dataAssets = {
            places:places
        }
        getDataByCategory(category , api , dataAssets);
    }

    renderFilter = () =>{
        const { category , api }:any = this.props;
        switch(category){
            case "places": return <PlacesFilter fetchPlaces={api.places.fetchPlaces} />
            default: return null
        }
    }

    renderCards = () =>{
        const { category , places }:any = this.props;
        switch(category){
            case "places": return <PlaceCardList places={places.data} />
            default: return null
        }
    }

    render(){
        const { places , category } :any = this.props;
        return (
            <Fragment>
                <HomeTabs category={category} />
                <Layout
                    filters={this.renderFilter()}
                    cards={
                        <div style={{padding:20}}>
                            <CardListHeader />
                            {this.renderCards()}
                        </div>}
                    map={<MapContainer places={places.data} />}
                    // map={"map"}
                />
            </Fragment>
        )
    }
}


const mapStateToProps = (state:any) => ({
    places: state.placesReducer
});

const mapDispatchToProps = (dispatch:any) => ({
    api: {
        places:{ 
            fetchPlaces:(search:object) => dispatch(loadPlacesAction(search)),
        }
    }
});

export default connect(mapStateToProps ,mapDispatchToProps)(Home);