import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';
import './assets/styles/normalize.scss';
import './assets/styles/index.scss';
import { ThemeProvider } from 'styled-components'
import theme from './theme.config';
import Root from './Root';

ReactDOM.render(
    <Root>
        <ThemeProvider theme={theme}>
            <Router>
                <App />
            </Router>
        </ThemeProvider>
    </Root>, 
    document.getElementById('root'));   