import React from "react";
import { Provider } from "react-redux";
import Redux, { applyMiddleware, createStore } from "redux";
import reduxLogger from "redux-logger";
import reduxThunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import reducers from "./store";

const middleware: Redux.Middleware[] = [reduxThunk];

if (process.env.NODE_ENV !== "production") {
  middleware.push(reduxLogger);
}

interface IRootProps {
  children?: React.ReactNode;
  initialState?: any;
}

export const store = createStore(
  reducers,
  {},
  composeWithDevTools(applyMiddleware(...middleware))
);


const Root: React.FC<IRootProps> = ({ children }) => {
  return <Provider store={store}>{children}</Provider>;
};

export default Root;
