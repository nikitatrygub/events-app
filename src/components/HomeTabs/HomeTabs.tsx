import React from 'react';
import { Grid } from '../Grid';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt , faCalendarWeek , faHotel } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'


interface HomeTabsProps {
    category:string
}

export const HomeTabs:React.FC<HomeTabsProps> = ({category}) => (
    <div className={styles.homeTabs}>
        <Grid fullwidth>
            <div className={styles.homeTabs__wrapper}>
                <div className={styles.homeTabs__left}>
                    <div className={styles.homeTabs__title}>
                        What are you looking for?
                    </div>
                </div>
                <div className={styles.homeTabs__right}>
                    <Link to="/explorer/places">
                        <button className={`${styles.homeTabs__tab} ${category === 'places' ? styles.homeTabs__tab__active : ''}`}>
                            <FontAwesomeIcon className={styles.homeTabs__tabIcon} size="lg" icon={faMapMarkerAlt} />
                            Places
                        </button>
                    </Link>
                    <Link to="/explorer/events">
                        <button className={`${styles.homeTabs__tab} ${category === 'events' ? styles.homeTabs__tab__active : ''}`}>
                            <FontAwesomeIcon className={styles.homeTabs__tabIcon}  size="lg" icon={faCalendarWeek} />
                            Events
                        </button>
                    </Link>
                    <Link to="/explorer/realestate">
                        <button className={`${styles.homeTabs__tab} ${category === 'realestate' ? styles.homeTabs__tab__active : ''}`}>
                            <FontAwesomeIcon className={styles.homeTabs__tabIcon}  size="lg" icon={faHotel} />
                            Real estate
                        </button>
                    </Link>
                </div>
            </div>
        </Grid>
    </div>
);