import React from 'react'
import styles from './styles.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilter , faBookmark } from '@fortawesome/free-solid-svg-icons'

export const FilterTabs:React.FC = () =>(
    <div className={styles.tabs}>
        <button className={`${styles.tabs__button}`}>
            <FontAwesomeIcon className={styles.tabs__button__icon} icon={faFilter} />
            Filters
        </button>
        <button className={styles.tabs__button}>
            <FontAwesomeIcon className={styles.tabs__button__icon} icon={faBookmark} />
            Categories
        </button>
    </div>
)