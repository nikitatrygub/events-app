import React from 'react';
import { PlaceCard } from '../PlaceCard/PlaceCard';

interface PlacesCardTypes {
    id:string
    fields:object
    createdTime:Date
}

interface PlaceCardListProps {
    places: PlacesCardTypes[]
}

export const PlaceCardList:React.FC<PlaceCardListProps> = ({places}) =>{
    const renderCards = () => {
        return places && places.map((item, i) =>(
            <PlaceCard id={item.id} fields={item.fields} key={i} />
        ))
    }
    return (
        <div style={{padding:'20px 0'}}>
            {renderCards()}
        </div>
    )
}