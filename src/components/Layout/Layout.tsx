import React from 'react';
import styles from './styles.module.scss'

interface LayoutProps {
    filters: React.ReactNode
    cards: React.ReactNode
    map: React.ReactNode
}

export class Layout extends React.Component<LayoutProps>{
    render(){
        const {filters , cards , map} = this.props; 
        return (
            <div className={styles.homeLayout}>
                <div className={styles.homeLayout__filters}>
                    {filters}
                </div>
                <div className={styles.homeLayout__items}>
                    {cards}
                </div>
                <div className={styles.homeLayout__mapbox}>
                    {map}
                </div>
            </div>
        )
    }
}