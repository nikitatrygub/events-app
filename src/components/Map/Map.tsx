import React from 'react'
import ReactMapboxGl from 'react-mapbox-gl';
import { MAP_TOKEN } from '../../env.config';
import mapStyles from './style.json';
import { MapMarker } from './MapMarker';

const Map = ReactMapboxGl({
    accessToken: `${MAP_TOKEN}`,
});

interface MapProps {
    places:any
}

export class MapContainer extends React.Component<MapProps>{

    state = {
       userLocation:[-0.481747846041145, 51.3233379650232]
    }

    componentDidMount(){
        this.getLocation()
    }

    setPostion = (position:any) =>{
        this.setState({
            userLocation:[position.coords.longitude , position.coords.latitude]
        })
    }

    getLocation(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(this.setPostion)
        }
        return null
    }

    renderLocations = () =>{
        const { places }:any = this.props;
        return places && places.map((place:any , i:number) => (
            <MapMarker key={i} place={place} />
        ))
    }

    render(){
        return (
            <div style={{width:'100%' , height:'100%'}}>
                <Map
                    style={mapStyles}
                    containerStyle={{
                        height: '100%',
                        width: '100%'
                    }}
                    center={[30.617078, 50.399203]}
                    zoom={[15]}
                >
                 {this.renderLocations()}  
                </Map>
            </div>
        )
    }
}