import React , { Fragment } from 'react';
import { Marker } from 'react-mapbox-gl';
import styles from './styles.module.scss';
import { getImageUrl } from '../../helpers/image.helpers';
import { PlaceMapCard } from '../PlaceMapCard/PlaceMapCard';

interface MapMarkerProps {
    place:object
}

interface MapMarkerState {
    isOpened:boolean
}

export class MapMarker extends React.Component<MapMarkerProps ,MapMarkerState>{
    state = {
        isOpened:false
    }

    handleOpen = () => {
        const {isOpened} = this.state;
        this.setState({
            isOpened:!isOpened
        })
    }

    render(){
        const {isOpened} = this.state;
        const {place:{fields}}:any = this.props;
        return (
            <Marker 
                coordinates={[fields.posY, fields.posX]}
                anchor="bottom"
                className={styles.container}
            >
                <Fragment>
                    {fields.image && <img onClick={this.handleOpen} className={styles.marker} src={getImageUrl(fields.logo)} alt={fields.title}/>}
                    <PlaceMapCard isOpened={isOpened} fields={fields} />
                </Fragment>
            </Marker>
        )
    }
}