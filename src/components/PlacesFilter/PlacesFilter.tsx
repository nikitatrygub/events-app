import React from 'react';
import styles from './styles.module.scss';
import { Input, Select, Checkbox, Button, ResetButton } from '../UI';
import { FilterTabs } from '../FilterTabs/FilterTabs';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const searchParams = {
    search:""
}

interface PlacesFilterProps {
    fetchPlaces:any
}

export class PlacesFilter extends React.Component<PlacesFilterProps>{

    state = searchParams

    handleChange = (value:string) =>{
        this.setState({
            search:value
        })
    }

    loadData = () =>{
        const { fetchPlaces } = this.props;
        fetchPlaces(this.state)
    }

    render(){
        const categoryOptions = [
            {value:"Nightlife"},
            {value:"Outdoor activities"},
            {value:"Cinemas"}
        ]
        const regionOprions = [
            {value:"London"},
            {value:"Amsterdam"}
        ]
        return(
            <div className={styles.placesFilter}>
                <FilterTabs />
                <Input label="What are you looking for?" text={(value:any)=> this.handleChange(value) } />
                <Select options={categoryOptions} label="All categories" />
                <Input label="Where to look?" locationInput />
                <Select options={regionOprions} label="Regions" />
                <div className={styles.placesFilter__title}>Tags</div>
                <Checkbox label="Accepts Credit Cards" />
                <Checkbox label="Bike parking" />
                <Checkbox label="Coupons" />
                <Checkbox label="Wireless internet" />
                <Button onClick={this.loadData} className={styles.placesFilter__button} icon={faSearch}>Search</Button>
                <ResetButton />
            </div>
        )
    }
}
