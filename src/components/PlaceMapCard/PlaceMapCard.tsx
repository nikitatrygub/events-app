import React from 'react';
import styles from './styles.module.scss';
import { getImageUrl } from '../../helpers/image.helpers';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faLocationArrow } from '@fortawesome/free-solid-svg-icons';

interface DataTypes {
    image:object
    title:string
    tel:string
    locName:string
}

interface PlaceMapCardProps {
    fields:DataTypes
    isOpened:boolean
}

export const PlaceMapCard:React.FC<PlaceMapCardProps> = ({fields , isOpened}) =>(
    <div className={`${styles.card} ${isOpened ? styles.card__opened : ''}`}>
        <div className={styles.card__overlay}></div>
        <div className={styles.card__bottom}>
            <div className={styles.card__title}>
                {fields.title}
            </div>
            <div className={styles.card__contacts}>
                <div className={styles.card__contacts__left}><FontAwesomeIcon icon={faPhone} /><span>{fields.tel}</span></div>
                <div className={styles.card__contacts__right}><FontAwesomeIcon icon={faLocationArrow} /><span>{fields.locName}</span></div>
            </div>
        </div>
        <img className={styles.card__img} src={getImageUrl(fields.image)} alt=""/>
    </div>
)
