import React from 'react';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faHeart, faUtensils, faPhone, faLocationArrow } from '@fortawesome/free-solid-svg-icons';
import { getImageUrl } from '../../helpers/image.helpers';

interface PlaceCardProps {
    id?:string
    fields:object
}

export const PlaceCard:React.FC<PlaceCardProps> = ({id , fields}) => {
    const { title, shortDesc, tel, locName, categoryName, image , logo } :any = fields;
    return (
        <div className={styles.card}>
            <div className={styles.card__img}>
                {image && <img src={getImageUrl(image , "large")}alt=""/>}
            </div>
            <div className={styles.card__description}>
                <img className={styles.card__logo} src={getImageUrl(logo)} alt=""/>
                <span className={styles.card__description__title}>{title}</span>
                <span className={styles.card__description__text}>{shortDesc}</span>
                <div className={styles.card__contacts}>
                    <div className={styles.card__contacts__left}><FontAwesomeIcon icon={faPhone} /><span>{tel}</span></div>
                    <div className={styles.card__contacts__right}><FontAwesomeIcon icon={faLocationArrow} /><span>{locName}</span></div>
                </div>
            </div>
            <div className={styles.card__bottom}>
                <div className={styles.card__category}>
                    <span className={styles.card__category__icon}>
                        <FontAwesomeIcon icon={faUtensils} />
                    </span>
                    <span className={styles.card__category__name}>{categoryName}</span>
                </div>
                <div className={styles.card__buttons}>
                    <button className={styles.card__button}>
                        <FontAwesomeIcon className={styles.card__button__icon} icon={faSearch} />
                    </button>
                    <button className={styles.card__button}>  
                        <FontAwesomeIcon className={styles.card__button__icon} icon={faHeart} />
                    </button>
                </div>
            </div>
        </div>
    )
}