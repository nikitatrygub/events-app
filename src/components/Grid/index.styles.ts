import styled from 'styled-components'

interface ContainerProps {
    readonly width?: Number
    readonly fullwidth?: Boolean
}

interface ColProps {
    lg?: Number
    md?: Number
    sm?: Number
    xs?: Number
}

const StyledContainer = styled.div<ContainerProps>`
    width:auto;
    max-width:${(props: any) => props.fullwidth ? "auto" : props.width ? props.width + 'px' : '1024px'};
    margin: 0 auto;
`

const StyledRow = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap:wrap;
    width: 100%;
`

const breakpoints = {
    large: 1390,
    medium: 1024,
    small: 760,
    xsmall: 500
}

const StyledCol = styled.div<ColProps>`
    width:${(props: any) => props.xs ? (100 / props.xs) + '%' : '100%'};
    padding:0 15px;
    ${(props) => {
        let { lg, md, sm, xs }: any = props
        let genBreakpoints = (ext: Number) => {
            switch (ext) {
                case 1: return 12
                case 2: return 6
                case 3: return 4
                case 4: return 3
                case 5: return 2.4
                case 6: return 2
                case 7: return 1.71428571429
                case 8: return 1.5
                case 9: return 1.33333333333
                case 10: return 1.200000048
                case 12: return 1
                default: return 1
            }
        }
        let mediaQuery = (bp: Number, value: Number) => `@media (min-width:${bp}px){
            width:${value ? (100 / genBreakpoints(value)) + '%' : '100%'};
        }`
        return `
            ${xs ? mediaQuery(breakpoints.xsmall, xs) : ''}
            ${sm ? mediaQuery(breakpoints.small, sm) : ''}
            ${md ? mediaQuery(breakpoints.medium, md) : ''}
            ${lg ? mediaQuery(breakpoints.large, lg) : ''}
        `
    }}
`

export { StyledContainer, StyledRow, StyledCol }