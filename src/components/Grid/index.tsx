import React from 'react'
import { StyledContainer , StyledRow , StyledCol } from './index.styles'

interface GridProps {
    width?:Number
    fullwidth?:Boolean
}

interface ColProps {
    lg?:Number
    md?:Number
    sm?:Number
    xs?:Number
}


export const Grid:React.FC<GridProps> = ({ width = 1180 , fullwidth, children }) =>(
    <StyledContainer width={width} fullwidth={fullwidth}>
        {children}
    </StyledContainer>
)

export const Row:React.FC = ({ children }) =>(
    <StyledRow>
        {children}
    </StyledRow>
)

export const Col:React.FC<ColProps> = ({ lg, md, sm, xs, children }) =>(
    <StyledCol lg={lg} md={md} sm={sm} xs={xs}>
        {children}
    </StyledCol>
)
