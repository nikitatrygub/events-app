import React from 'react';
import styles from './styles.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSort } from '@fortawesome/free-solid-svg-icons';

export const CardListHeader = () =>(
    <div className={styles.cardListHeader}>
        <div>
            <button className={styles.cardListHeader__button}>
                <FontAwesomeIcon className={styles.cardListHeader__button__icon} icon={faSort} />
                Date
            </button>
        </div>     
        <div className={styles.cardListHeader__results}>
            10 results
        </div>       
    </div>
)