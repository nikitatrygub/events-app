import React, { Fragment } from 'react';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCompass } from '@fortawesome/free-solid-svg-icons';

interface InputProps {
    label?: string
    locationInput?:boolean
    text?:any
}

export class Input extends React.Component<InputProps> {
    state = {
        value:""
    }
    handleChange = (e:any) =>{
        const { text } = this.props;
        this.setState({
            value: e.target.value
        })
        text && text(e.target.value)
    }
    render(){
        const { value } = this.state;
        const { label , locationInput } = this.props;
        return (
            <Fragment>
                <div className={styles.inputBox} style={{marginTop:25}}>
                    {locationInput && <FontAwesomeIcon className={styles.locationIcon} icon={faCompass} />}
                    <input className={styles.input} type='text' value={value} onChange={this.handleChange} />
                    <label className={`${styles.label} ${value.length > 0 ? styles.label__toggled : ''}`}>{label}</label>
                </div>
            </Fragment>
        )
    }
}
