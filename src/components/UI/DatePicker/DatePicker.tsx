import "date-fns"
import React from 'react';
import DatePicker from 'react-datepicker';
import "./DatePicker.styles.scss";
import styles from "./styles.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";

interface DatepickerProps {
    label?:string
    selected?:any
}

interface DatepickerState {
    date:Date
}

export class Datepicker extends React.Component<DatepickerProps , DatepickerState>{

    state = {
        date:new Date()
    }

    handleChange = (date:Date) =>{
        const { selected } = this.props;
        this.setState({
            date:date
        })

        selected && selected(date)
    }

    render(){
        const { date } = this.state;
        const { label } = this.props;
        return (
            <button className={styles.datepickerButton}>
                <span className={styles.datepickerButton__content}>
                    <FontAwesomeIcon className={styles.datepickerButton__icon} icon={faCalendar}/>
                    <p className={styles.datepickerButton__label}>{label && label}</p>
                </span>
                <DatePicker
                    className={styles.customDatepicker} 
                    selected={date}
                    onChange={this.handleChange} 
                />
            </button>
        )
    }
}
