import React from 'react';
import styles from  "./styles.module.scss";

interface InputRangeProps {
    minValue?:number
    maxValue?:number
    value?:any
}

interface InputRangeState {
    min:number
    max:number
}

export class InputRange extends React.Component<InputRangeProps, InputRangeState>{

    constructor(props:any){
        super(props);
        this.state = {
            min:props.minValue,
            max:props.maxValue
        }
    }

    handleChangeMin = (e:any) =>{
        const { value } = this.props;
        const { min, max } = this.state;
        this.setState({
            min:Number(e.target.value) >= max ? max : Number(e.target.value)
        } , () => value && value({min:min , max:max}))
        
    }

    handleChangeMax = (e:any) =>{
        const { value } = this.props;
        const { max, min } = this.state;
        this.setState({
            max:Number(e.target.value) <= min ? min : Number(e.target.value)
        } , () => value && value({min:min ,max:max}))
     
    }

    render(){
        const { min, max } = this.state;
        const { minValue, maxValue } = this.props;
        return (
            <div className={styles.wrapper}>
                <input className={styles.input__min} value={min} min={minValue} max={maxValue} onChange={this.handleChangeMin} type="range" name="min" />
                <input className={styles.input__max} value={max} min={minValue} max={maxValue} onChange={this.handleChangeMax} type="range" name="max" />
            </div>
        )
    }
}