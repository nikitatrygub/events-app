import React from 'react';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

interface CheckboxProps {
    label?:string
    handleClick?: any 
}

export class Checkbox extends React.Component<CheckboxProps>{
    state = {
        checked:false
    }

    handleChange = () =>{
        const { handleClick } = this.props;
        const { checked } = this.state;
        this.setState({
            checked:!checked
        })
        handleClick && handleClick(!checked)
    }

    render(){
        const { checked } = this.state;
        const { label } = this.props;
        return (
            <div className={styles.checkboxContainer}>
                <span onClick={this.handleChange} className={`${styles.box} ${checked ? styles.box__checked : ''}`}><FontAwesomeIcon className={`${styles.box__icon} ${checked ? '' : styles.box__icon__hide}`} icon={faCheck} /></span>
                <input className={styles.checkbox} type="checkbox" id="checkbox" />
                <label onClick={this.handleChange} className={styles.label} htmlFor="checkbox">{label && label}</label>
            </div>
        )
    }
}
