import React from 'react';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo } from '@fortawesome/free-solid-svg-icons';

interface ResetButtonState {
    isReloading:boolean
}

export class ResetButton extends React.Component<{},ResetButtonState>{
    timeout: any
    constructor(props:any){
        super(props);
        this.state = {
            isReloading:false
        }
    }

    componentDidMount(){
        this.timeout = null;
    }

    handleChange = () => {
        this.setState({
            isReloading:true
        })
        
        this.timeout = setTimeout(()=>{
            this.setState({
                isReloading:false
            })
        },2000)
    }

    componentWillUnmount(){
        clearTimeout(this.timeout);
    }

    render(){
        const {isReloading} = this.state;
        return (
            <button 
                onClick={this.handleChange} 
                className={`${styles.button}`}>
                <FontAwesomeIcon 
                    className={`${styles.button__icon} ${isReloading ? styles.button__rotate : ''}`} 
                    icon={faRedo} 
                />
                Reset
            </button>
        )
    }
}