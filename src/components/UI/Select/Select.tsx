import React from 'react';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

interface option {
    value?:string
}

interface SelectState {
    isOpen:boolean
    selected:any
}

interface SelectProps {
    options?: option[]
    label?: string
    selected?:any
}

export class Select extends React.Component<SelectProps , SelectState>{
    wrapperRef: any;

    constructor(props:any) {
        super(props);
        this.state = {
            isOpen:false,
            selected:null
        }
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    setWrapperRef(node:any) {
        this.wrapperRef = node;
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }
    
    handleClickOutside(event:any) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
          this.setState({
              isOpen:false
          })
        }
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    toggleSelect = () =>{
        this.setState({
            isOpen:true
        })
    }

    select = (value:any) =>{
        this.setState({
            isOpen:false,
            selected:value
        })
        this.props.selected && this.props.selected(value)
    }

    renderOptions = () =>{
        const {options} = this.props;
        return options && options.map((option,i)=>(
            <li onClick={()=>this.select(option.value)} key={i}>{option.value}</li>
        ))
    }

    render(){
        const { isOpen , selected } = this.state;
        const { label } = this.props;
        return (
            <div ref={this.setWrapperRef} className={styles.selectBox} style={{marginTop:25}}>
                <FontAwesomeIcon className={styles.arrow} icon={faChevronDown} />
                <input readOnly onClick={this.toggleSelect} className={styles.select} value={selected ? selected : ''} type="text" name="select-input" />
                <label className={`${styles.label} ${selected ? styles.label__toggled : ''}`} htmlFor="select-input">{label && label}</label>
                <ul className={`${styles.dropdown} ${isOpen ? styles.dropdown__toggled : ''}`}>
                    {this.renderOptions()}
                </ul>
            </div>
        )
    }
}