import React from 'react';
import styles from './styles.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface ButtonProps {
    icon?:any
    className?:string
    onClick?:any
}

export const Button:React.FC<ButtonProps> = ({onClick , children , icon , className}) =>{
    return (
        <button onClick={onClick} className={`${styles.button} ${className ? className : ''}`}>
            <span className={styles.button__text}>{children}</span>
            {icon && <FontAwesomeIcon className={styles.button__icon} icon={icon} />}
        </button>
    )
}