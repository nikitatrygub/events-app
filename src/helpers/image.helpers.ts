export const getImageUrl = (image: any, size?:string) => {
    const img = image[0];
    const { thumbnails } = img;
    if (image && size === 'small') {
        return thumbnails.small.url
    }
    if (image && size === 'large') {
        return thumbnails.large.url
    }
    return thumbnails.full.url
}