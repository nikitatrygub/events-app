const MAP_TOKEN = process.env.REACT_APP_MAP_ACCESS_TOKEN;
const API_BREAKPOINT = 'https://api.airtable.com/v0/appheKbhRUiZYfI2D';
const API_KEY = process.env.REACT_APP_API_KEY;

export { MAP_TOKEN, API_BREAKPOINT, API_KEY }