import { API_BREAKPOINT, API_KEY } from "../../env.config";

export const getPlaces = async () => {
    const resp = await fetch(`${API_BREAKPOINT}/places`, {
        method: "GET",
        headers: {
            Authorization: `Bearer ${API_KEY}`,
            "Content-Type": "application/json"
        }
    })
    if (resp.ok) {
        const places = await resp.json();
        return places.records;
    } else {
        const error = await resp.json();
        throw Error(error)
    }
}

export const filterPlaces = async (searchParams:object) =>{
    const { search } :any = searchParams;
    const resp = await fetch(`${API_BREAKPOINT}/places?filterByFormula=(SEARCH('${search}',{title}))`, {
        method: "GET",
        headers: {
            Authorization: `Bearer ${API_KEY}`,
            "Content-Type": "application/json"
        }
    })
    if (resp.ok) {
        const places = await resp.json();
        return places.records;
    } else {
        const error = await resp.json();
        throw Error(error)
    }
}