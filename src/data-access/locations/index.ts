import { API_BREAKPOINT, API_KEY } from "../../env.config";

export const getLocationsByCategory = async (category: string) => {
    const resp = await fetch(`${API_BREAKPOINT}/locations?filterByFormula=({type}='${category}')`, {
        method: "GET",
        headers: {
            Authorization: `Bearer ${API_KEY}`,
            "Content-Type": "application/json"
        }
    })
    if (resp.ok) {
        const locations = await resp.json();
        return locations.records
    } else {
        const error = await resp.json();
        throw Error(error);
    }
}