import { LOAD_DATA, LOAD_SUCCESSFUL, LOAD_ERROR } from "./types";
import { getPlaces, filterPlaces } from "../../data-access/places";

export const loadPlacesAction = (search?: object) => {
    return async (dispatch: any, getState: any) => {
        dispatch({
            type: LOAD_DATA
        })
        try {
            if (search) {
                let places = await filterPlaces(search);
                if (places) dispatch({
                    type: LOAD_SUCCESSFUL,
                    payload: places
                })
            }
            else {
                let places = await getPlaces();
                if (places) dispatch({
                    type: LOAD_SUCCESSFUL,
                    payload: places
                })
            }
        } catch (e) {
            dispatch({
                type: LOAD_ERROR,
                payload: e.message
            })
        }
    }
}

