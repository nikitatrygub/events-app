import { produce } from 'immer';
import { LOAD_DATA, LOAD_SUCCESSFUL, LOAD_ERROR } from './types';

const initialState = {
    data: [],
    isLoading: true,
    isError: false
}

export function placesReducer(state = initialState, action: any) {
    return produce(state, draft => {
        switch (action.type) {
            case LOAD_DATA: {
                draft.isLoading = true
                break;
            }
            case LOAD_SUCCESSFUL:{
                draft.isLoading = false
                draft.data = action.payload
                break;
            }
            case LOAD_ERROR:{
                draft.isLoading = false
                draft.isError = action.payload
                break;
            }
            default: return state;
        }
    })
}