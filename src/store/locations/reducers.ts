import { produce } from 'immer';
import { LOAD_LOCATIONS, LOAD_LOCATIONS_SUCCESSFUL, LOAD_LOCATIONS_ERROR } from './types';

const initialState = {
    data: [],
    isLoading: true,
    isError: false
}

export function locationsReducer(state = initialState, action: any) {
    return produce(state, draft => {
        switch (action.type) {
            case LOAD_LOCATIONS: {
                draft.isLoading = true
                break;
            }
            case LOAD_LOCATIONS_SUCCESSFUL:{
                draft.isLoading = false
                draft.data = action.payload
                break;
            }
            case LOAD_LOCATIONS_ERROR:{
                draft.isLoading = false
                draft.isError = action.payload
                break;
            }
            default: return state;
        }
    })
}