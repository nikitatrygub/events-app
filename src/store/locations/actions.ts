import { LOAD_LOCATIONS, LOAD_LOCATIONS_SUCCESSFUL, LOAD_LOCATIONS_ERROR } from "./types";
import { getLocationsByCategory } from "../../data-access/locations";

export const loadLocationsAction = (category: string) => {
    return async (dispatch: any, getState: any) => {
        dispatch({
            type: LOAD_LOCATIONS
        })

        try {
            const locations = await getLocationsByCategory(category)
            if (locations) dispatch({
                type: LOAD_LOCATIONS_SUCCESSFUL,
                payload: locations
            })
        } catch (e) {
            dispatch({
                type: LOAD_LOCATIONS_ERROR,
                payload: e.message
            })
        }
    }
}