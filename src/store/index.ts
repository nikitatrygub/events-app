import { placesReducer } from "./places/reducers";
import { combineReducers } from "redux";
import { locationsReducer } from "./locations/reducers";


const rootReducer = combineReducers({ placesReducer, locationsReducer })

export default rootReducer